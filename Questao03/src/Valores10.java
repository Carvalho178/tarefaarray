import java.util.Arrays;
import java.util.Scanner;

class Valores10 {
	public static void main(String[] args) {
		int Valores[] = new int[10];
		int pesquisa;
		Scanner leitor = new Scanner(System.in);
		for (int v: Valores) {
			Valores[v] = leitor.nextInt();
		}
		Arrays.sort(Valores);
		
		System.out.println("Pesquise");
		pesquisa = leitor.nextInt();
		int local = Arrays.binarySearch(Valores, pesquisa);
		System.out.println(local);
		
		leitor.close(); 
	}
}