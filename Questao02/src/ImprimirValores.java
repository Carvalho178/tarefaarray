
public class ImprimirValores {
	public static void main(String[] args) {
		Integer ArrayInteiro[] = new Integer[300];
		for (int i = 0; i<300; i++) {
			ArrayInteiro[i] = 45;
		}
		//primeira forma
		for(int i = 0; i<300; i++) {
			System.out.println("Campo "+ (i+1) + " = " +ArrayInteiro[i]);
		}
		//segunda forma
		int i = 0;
		while(i<300) {
			System.out.println("Campo "+ (i+1) + " = " +ArrayInteiro[i]);
			i++;
		}
		i=0;
		//terceira forma
		do {
			System.out.println("Campo "+ (i+1) + " = " +ArrayInteiro[i]);
			i++;
		}while(i<300);
	}
	
}
